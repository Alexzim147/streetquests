import React from 'react';
import { Grid, Paper, Typography } from "@material-ui/core";
import '../style/fonts.css'
import '../style/rectangles.css'

function Question({text}){
    return (
        <div>
            <Grid item xs={8} align='left'>
                <Grid xs={12}>
                    <Typography class='mda1'>
                        Вопрос
                    </Typography>
                </Grid>
                <Paper class='rectangle4'>
                    <Grid item xs={12} align='center'>
                        <Typography class='question_text'>
                            {text}
                        </Typography>
                    </Grid>
                </Paper>
            </Grid>
        </div>
    )
}

export default Question