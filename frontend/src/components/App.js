import React from 'react';
import Home from './Home'
import Game from './Game'
import NotFound from './NotFound'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

function App(){
    return (
        <div>
            <Router>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route exact path='/game:number'component={Game}/>
                    <Route component={NotFound}/>
                </Switch>
            </Router>
        </div>
    )
}

export default App