import React from 'react';
import { Grid, Paper, Typography } from "@material-ui/core";
import '../style/fonts.css'
import '../style/rectangles.css'

function NotFound(){
    return (
        <div>
        <Grid container>
        <Grid item xs={12} align="center">
            <Paper class='rectangle4'>
                    <Typography class={'mda1'}>
                        Такой страницы нет
                    </Typography>
                    <a href='https://www.youtube.com/watch?v=dQw4w9WgXcQ&ab_channel=RickAstleyVEVO'>
                        <Typography class={'mda1'}>
                            Вернуться на главную
                        </Typography>
                    </a>
            </Paper>
            </Grid>
        </Grid>
        </div>
    )
}

export default NotFound