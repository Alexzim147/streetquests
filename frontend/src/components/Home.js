import React from 'react';
import { Grid, Paper, Typography } from "@material-ui/core";
import '../style/background_picture.css'
import '../style/fonts.css'
import '../style/rectangles.css'
import Footer from './Footer';
import Numbers from './Numbers';

function Home(){
    return (
        <div>
            <Paper className='picture'>
                <Grid container >
                    <Grid item xs={12} align="center">
                        <Typography class={'main_header'}>
                            ПЕШЕХОДНЫЕ КВЕСТЫ<br />ПО ВСЕЙ РОССИИ
                        </Typography>
                    </Grid>
                    <Grid item xs={12} align="center">
                        <Typography className={'under_main_header'}>
                            играй, изучай, развивайся, развлекайся
                        </Typography>
                    </Grid>
                </Grid>
            </Paper >
            <Paper className='rectangle0'>
                <Grid container spacing={0}>
                    <Grid item xs={12}>
                        <Grid container spacing={0} justify="center">
                            <Grid item xs={12}>
                                <Typography class={'mda1'}>
                                    Попробуй квест в своем городе!
                                </Typography>
                            </Grid>
                            <Grid item sm={12} md={4} align='center'>
                                <a href='/game1'>
                                    <img src='img/moscow.jpg' alt='perm'/>
                                </a>
                                <Typography class={'mda1'}>
                                    Москва
                                </Typography>
                            </Grid>
                            <Grid item sm={12} md={4} align='center'>
                                <a href='/game2'>
                                    <img src='img/piter.jpg' alt='perm'/>    
                                </a>
                                <Typography class={'mda1'}>
                                    Санкт-Петербург
                                </Typography>
                            </Grid>
                            <Grid item sm={12} md={4} align='center'>
                                <a href='/game3'>
                                    <img src='img/perm.jpg' alt='perm'/>
                                </a>
                                <Typography class={'mda1'}>
                                    Пермь
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Footer style_name='rectangle3'/>
                    </Grid>
                </Grid>
            </Paper>
        </div>
    )
}

export default Home