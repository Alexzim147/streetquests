import React from 'react';
import { Grid, Paper, Typography } from "@material-ui/core";
import '../style/fonts.css'
import '../style/rectangles.css'

function Numbers(props){
    const {cities, quests, reviews} = props
    return (
        <div>
            <Paper className={'rectangle2'}>
                    <Grid container >
                        <Grid item xs={12} sm={12} md={4}>
                            <Typography className={'mda'}>
                                {cities}
                            </Typography>
                            <Typography className={'mda1'}>
                                города
                            </Typography>
                        </Grid>
                        <Grid  item xs={12} sm={12} md={4}>
                            <Typography className={'mda'}>
                                {quests}
                            </Typography>
                            <Typography className={'mda1'}>
                                квестов
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={12} md={4}>
                            <Typography className={'mda'}>
                                {reviews}
                            </Typography>
                            <Typography className={'mda1'}>
                                отзывов
                            </Typography>
                        </Grid>
                    </Grid>
            </Paper>
        </div>
    )
}

export default Numbers