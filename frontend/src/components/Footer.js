import React from 'react';
import { Grid, Paper, Typography } from "@material-ui/core";
import '../style/rectangles.css'


function Footer(props){
    const {style_name} = props
    return (
        <Paper class={style_name}>
            <Grid container>
                <Grid item xs={6} align='center'>
                    <Typography class='footer_text'>
                        © 2020 Пешеходные квесты
                    </Typography>
                </Grid>
                <Grid item xs={6} align='center'>
                    <Typography class='footer_text'>
                        Разработано Александром Зименковым
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default Footer