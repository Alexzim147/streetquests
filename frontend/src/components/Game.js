import React, { useState } from 'react';
import { Grid, Paper, Button } from "@material-ui/core";
import '../style/background_picture.css'
import '../style/fonts.css'
import '../style/rectangles.css'
import '../style/input.css'
import {Context} from '../context'
import Question from './Question';

var question_number = 0

function Game(number){
    var user_answer = ''
    var questions = [
        {'question': 'Что изображено на фотографии?', 'answer': 'Галерея'},
        {'question': 'Какой город был основан в 1723 году?', 'answer': 'Пермь'}
    ]

    const [current_question, setQuestion] = useState(questions[0])

    const handleChange = event=>{
        const value = event.target.value
        user_answer = value
    }

    const handleSubmit = event=>{
        if (event.key === 'Enter') {
            handleClick(event)
        }
    }

    const handleClick = event=>{
        if (user_answer === current_question['answer']) {
            question_number += 1 
            console.log(question_number)
            if (question_number < questions.length) {
                changeQuestion(questions[question_number])
            } else {
                var victory = {'question': "Вы прошли квест!", 'answer': ''}
                changeQuestion(victory)
            }
        } else {
            
        }
    }

    const changeQuestion = (question)=>setQuestion({
        'question': question['question'],
        'answer': question['answer']
    })

    return (
        <Context.Provider value={{changeQuestion}}>
            <div>
                <Grid container spacing={4}>
                    <Grid item xs={6} align='center'>
                        <img src='img/perm.jpg' alt='perm'/>
                    </Grid>
                    <Grid item xs={6}>
                        <Question text={current_question['question']}/>
                    </Grid>

                    <Grid item xs={12} align='center'>
                        <input name='Введите ответ' class={'input'} onChange={handleChange} onKeyDown={handleSubmit}>
                        </input>
                    </Grid>
                    <Grid container item xs={12}>
                        <Paper class='rectangle5'>
                            <Grid item xs={6} align='center'>
                                <Button class='rectangle6' onClick={handleClick}>
                                    Отправить ответ
                                </Button>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        </Context.Provider>
    )
}

export default Game